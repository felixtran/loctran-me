import React from 'react'
import PropTypes from 'prop-types'

import { i18n, Link, withTranslation } from '../i18n'

import Header from '../components/Header'
import Footer from '../components/Footer'

const language = [
  {
    key: 'en',
    text: 'English',
  },
  {
    key: 'de',
    text: 'Deutch',
  },
  {
    key: 'vi',
    text: 'Tiếng Việt',
  }
]


const Homepage = ({ t }) => (
  <React.Fragment>
    <main>
      <Header title={t('h1')} />
      <div>
        {language.map(e => (
          <button
            type='button'
            onClick={() => i18n.changeLanguage(e.key)}
          >
            {e.text}
          </button>
        ))}
        <Link href='/second-page'>
          <button
            type='button'
          >
            {t('to-second-page')}
          </button>
        </Link>
      </div>
    </main>
    <Footer />
  </React.Fragment>
)

Homepage.getInitialProps = async () => ({
  namespacesRequired: ['common', 'footer'],
})

Homepage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('common')(Homepage)
